﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;
public class Fps : MonoBehaviour
{
    float deltaTime = 0.0f;
    public Text fpsText;
    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }
    private void Start()
    {
        XRSettings.eyeTextureResolutionScale = 1.3f;
        //QualitySettings.antiAliasing = 2;
    }
    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        fpsText.text = fps.ToString();
    }
}
