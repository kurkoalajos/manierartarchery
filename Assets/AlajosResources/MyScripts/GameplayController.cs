﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
public class GameplayController : BaseBehaviour
{
    [Header("XRRig player object")]
    [SerializeField]
    private GameObject XRRigPlayer;
    [Header("Bow GameObject")]
    [SerializeField]
    private GameObject bow;
    [Header("Gives the startinposition of the player")]
    [SerializeField]
    private GameObject startingPosition;
    [Header("If it is true the UI objects will appear")]
    [Space]
    public bool showUI;
    public XRController controller = null;

    private void Start()
    {
        controller = GetComponent<XRController>();
        StartCoroutine(InitLevel());
    }
    public IEnumerator InitLevel()
    {
        yield return new WaitForSeconds(.1f);
        ShowIntro();
    }
    public void StartGame()
    {
        HideIntro();
    }
    public void GameOver()
    {
        MainGameController.currentGamestate = MainGameController.Gamestates.gameOver;
    }
    public void ShowIntro()
    {
        MainGameController.currentGamestate = MainGameController.Gamestates.intro;
        XRRigPlayer.transform.position = startingPosition.transform.position;
        showUI = true;
    }
    public void HideIntro()
    {
        MainGameController.currentGamestate = MainGameController.Gamestates.play;
        XRRigPlayer.transform.position = new Vector3(1.17f, 5f, 8.99f);
        showUI = false;
    }
    private void Update()
    {
        if (showUI == true)
        {
            bow.SetActive(false);
        }
        else
        {
            bow.SetActive(true);
        }
    }
}
