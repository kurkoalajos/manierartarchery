﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TatarWarrior : BaseBehaviour
{
    public bool isLeader = false,isRunning = false;
    [SerializeField]
    private Animator tatarAnimator;
    [SerializeField]
    private GameObject soldiers;
    private System.Random rand;

    public bool isRunningRight;
    public float speed = 0;
    public float animDelay = 0;
    // Start is called before the first frame update

    // Update is called once per frame
    private void Start()
    {
        StartCoroutine(FightAnimation());
    }
    private IEnumerator FightAnimation()
    {
        yield return new WaitForSecondsRealtime(animDelay);
        tatarAnimator.SetBool("Play", true);
    }
    public void GetHit()
    {
        if(speed == 0)
        {
            speed = 0.1f;
        }
        if (isLeader)
        {
            MainGameController.currentGamestate = MainGameController.Gamestates.gameFinished;
            StartCoroutine(PlayDieAnimation());
            StartCoroutine(PlayRunAwayAnimationForSoldiers());
        }
        else
        {
            PlayDieAnimation();
        }
    }
    private IEnumerator PlayDieAnimation()
    {
        tatarAnimator.Play("Dead2");
        yield return new WaitForSeconds(3.5f);
        tatarAnimator.enabled = false;
    }
    private IEnumerator PlayRunAwayAnimationForSoldiers()
    {
        for (int i = 0; i < soldiers.transform.childCount; i++)
        {
            var tatarScript = soldiers.transform.GetChild(i).GetComponent<TatarWarrior>();
            tatarScript.RunAway();
        }
        //soldiers.GetComponentInChildren<Animator>().Play("Running_mb");
        yield return new WaitForSeconds(0);
    }

    private void RunAway()
    {
        tatarAnimator.Play("Running_mb");
        isRunning = true;
    }
    private void Update()
    {
        if(isRunning == true)
        {
            if (isRunningRight)
            {
                this.transform.position = new Vector3(this.transform.position.x + speed, this.transform.position.y, this.transform.position.z);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x - speed, this.transform.position.y, this.transform.position.z);
                this.transform.rotation = Quaternion.Euler(this.transform.rotation.x, -77, this.transform.rotation.z);
            }
            
        }
    }
}
