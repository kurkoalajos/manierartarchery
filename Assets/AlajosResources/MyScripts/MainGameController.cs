﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameController : BaseBehaviour
{

    [SerializeField]
    private GameplayController _gameplayController;
    public static MainGameController Instance;

    public static GameplayController GameplayController;

    public Gamestates currentGamestate;
    public enum Gamestates
    {
        intro,
        play,
        gameOver,
        gameFinished
    }

    
    // Start is called before the first frame update

    void Awake()
    {
        Instance = this;
        GameplayController = _gameplayController;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
