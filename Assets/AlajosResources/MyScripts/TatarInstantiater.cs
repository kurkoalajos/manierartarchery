﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TatarInstantiater : MonoBehaviour
{
    [SerializeField]
    private GameObject tatarWarriorPrefab;
    private GameObject tatarwarrior;
    public void InstantiateTatars()
    {
        float xPos = -80, zPos = 13;
        for (int i = 0; i < 300; i++)
        {
            if(xPos < 80)
            {
                xPos += 5;
            }
            else
            {
                xPos = -80;
                zPos -= 8;
            }
            
            tatarwarrior = Instantiate(tatarWarriorPrefab, this.transform);
            tatarwarrior.transform.localPosition = new Vector3(xPos, 1.49f, zPos);
            tatarwarrior.GetComponent<TatarWarrior>().isLeader = false;
        }
        
    }

}
