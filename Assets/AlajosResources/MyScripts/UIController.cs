﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

public class UIController : BaseBehaviour
{
    [SerializeField]
    private GameObject startGameCanvas,gameFinishedCanvas;
    [SerializeField]
    private GameObject rightUIInteractor;
    [SerializeField]
    private Animator cubeFadeInAnim;

    private void Awake()
    {
        cubeFadeInAnim.Play("FadeOut");
    }
    public void StartGame()
    {
        MainGameController.GameplayController.StartGame();
    }
    public void RestartGame()
    {
        StartCoroutine(FadeinAnim());
        
        //MainGameController.GameplayController.InitLevel();
    }
    private void ReloadScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    private IEnumerator FadeinAnim()
    {
        cubeFadeInAnim.Play("FadeIn");
        yield return new WaitForSeconds(0.3f);
        ReloadScene();
        
    }
    private void Update()
    {
        if (MainGameController.currentGamestate == MainGameController.Gamestates.intro)
        {
            rightUIInteractor.SetActive(true);
            startGameCanvas.SetActive(true);
            gameFinishedCanvas.SetActive(false);
        }
        else if(MainGameController.currentGamestate == MainGameController.Gamestates.gameFinished)
        {
            rightUIInteractor.SetActive(true);
            gameFinishedCanvas.SetActive(true);
        }
        else if(MainGameController.currentGamestate == MainGameController.Gamestates.play)
        {
            rightUIInteractor.SetActive(false);
            startGameCanvas.SetActive(false);
            gameFinishedCanvas.SetActive(false);
        }
    }
}
